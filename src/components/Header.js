import React from 'react'
import { Navbar } from 'react-bootstrap'
import { Offline, Online } from 'react-detect-offline'

const Header = () => {
  return (
    <Navbar variant='dark' bg='primary'>
      <Navbar.Brand>
        کارام
      </Navbar.Brand>
      <Navbar.Toggle />
      <Navbar.Collapse className='justify-content-end'>
        <Navbar.Text>
          <Online>آنلاینی</Online>
          <Offline>آفلاینی</Offline>
        </Navbar.Text>
      </Navbar.Collapse>
    </Navbar>
  )
}

export { Header }
