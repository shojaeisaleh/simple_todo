import React, { useState, useEffect } from 'react'
import { Button, InputGroup, FormControl, Container, ListGroup } from 'react-bootstrap'
import { Header } from './components'

const App = () => {
  const [ value, setValue ] = useState('')
  const [ list, setList ] = useState(
    localStorage.getItem('list') ?
    JSON.parse(localStorage.getItem('list')) : []
  )

  const handelSubmit = e => {
    e.preventDefault()
    setList(prev => [...prev, {
      id: list.length,
      text: value
    }])
    setValue('')
  }

  const handelRemove = id => {
    const new_list = list.filter(item => item.id !== id)
    setList(new_list)
  }

  useEffect(() => {
    localStorage.setItem('list', JSON.stringify(list))
  }, [ list ])

  return (
    <>
      <Header />
      <Container className='mt-4'>
        <form onSubmit={e => handelSubmit(e)}>
          <InputGroup>
            <FormControl
              placeholder='کار من...'
              value={value}
              onChange={e => setValue(e.target.value)}
            />
            <InputGroup.Prepend>
              <Button type='submit' variant='success'>
                افزودن
              </Button>
            </InputGroup.Prepend>
          </InputGroup>
        </form>
        {
          list.length ?
            <ListGroup className='mt-4 text-right'>
            {
              list.map(item =>
                <ListGroup.Item
                  key={item.id}
                  className='d-flex justify-content-between align-items-center'
                > { item.text }
                  <Button
                    size='sm'
                    variant='danger'
                    onClick={() => handelRemove(item.id)}
                  >
                    حذف
                  </Button>
                </ListGroup.Item>
              )
            }
            </ListGroup>
          : <div className='mt-4 text-center text-success'>
              هورا! تمام کارای لعنتیت تموم شد!
          </div>
        }
      </Container>
    </>
  )
}

export default App
